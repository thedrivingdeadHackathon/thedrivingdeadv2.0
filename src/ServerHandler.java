import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.regex.Pattern;

/**
 * Created by jusic on 31.03.2017.
 */
public class ServerHandler extends Thread {

    private String pos;
    private String[] spos;
    private int[] ipos;
    private PrintWriter out;
    private BufferedReader in;
    private Socket clientSocket;
    private Auto auto, freund;

    public ServerHandler(Auto auto, Auto freund) {
        this.auto = auto;
        this.freund = freund;
    }


    public void run() {
        try {

            clientSocket = new Socket("10.72.3.131", 6689);
            out = new PrintWriter(clientSocket.getOutputStream(), true);;
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        while (clientSocket != null) {
            try {

                out.println(auto.getX() + " " + auto.getY());

                pos = in.readLine();
                spos = pos.split(Pattern.quote(" "));
                ipos[0] = Integer.parseInt(spos[0]);
                ipos[1] = Integer.parseInt(spos[1]);

                freund.setX(ipos[0]);
                freund.setY(ipos[1]);

                
                sleep(200);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
