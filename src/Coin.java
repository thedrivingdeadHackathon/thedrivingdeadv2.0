import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

/**
 * Created by jusic on 31.03.2017.
 */
public class Coin extends SpielObjekt{

    private Shape hitbox;
    private Rectangle rect;
    private boolean used = false;
    private float scale = 1;
    private boolean scaledir = true ;

    //Animation
    private Animation drehen;
    private SpriteSheet spriteSheet;
    int[] times = {150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150};

    public Coin(int x, int y, Image image) throws SlickException {
        super(x, y, image);
        rect = new Rectangle(getX(), getY(), 24, 24);
        hitbox = rect;

        spriteSheet = new SpriteSheet("res/coin24.png", 24, 24);
        drehen = new Animation(spriteSheet, new int[]{0, 0, 1, 0, 5, 0, 10, 0, 15, 0, 20, 0, 25, 0, 30, 0, 35, 0 ,40, 0, 45, 0, 50, 0}, times);
    }

    public void draw(Graphics g) {

        if (!used) {
            drehen.draw(x, y ); //animation schlad grawing
        }
        // g.draw(rect);
    }

    public void update(int delta) {
        if(scaledir){
            scale -= 0.01;
            if(scale <= 0.5){
                scaledir = false;
            }
        }
        if(!scaledir) {
            scale += 0.01;
            if(scale >= 1){
                scaledir = true;
            }
        }
    }

    public Shape getHitbox() {
        return hitbox;
    }


    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

}
