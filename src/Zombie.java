import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

import java.util.Random;

/**
 * Created by jusic on 31.03.2017.
 */
public class Zombie extends SpielObjekt {

    //EIGENSCHAFTEN
    private float leben = 100;
    private int a, b; //
    private float angle;
    private boolean aggro = false;
    private float geschw;
    private boolean alive = true;
    private boolean diedOnce = false;
    private int aliv = 0;


    //OBJEKTE
    private Shape hitbox;
    private Rectangle rect;
    private Transform transf;
    private Random r;
    private Punkte points;

    //Animation
    private int pos = 0;
    private Animation schlag;
    private SpriteSheet spriteSheet;
    int[] times = {150, 150, 150, 150, 150, 150, 150, 150};
    private Image deadZombie;



    //HILFSVARIABLE
    private double cos;
    private double sin;

    /**
     * Konstruktor des Zombies. Hier wird der Zombie erstellt, seine Position definiert und das Bild geladen
     *
     * @param x     int - x koordinate des Zombies
     * @param y     int - y koordinate des Zombies
     * @param image Image - Bild des Zombies
     */
    public Zombie(int x, int y, Image image, Punkte points) throws SlickException {
        super(x, y, image);

        rect = new Rectangle(getX(), getY(), 32, 29);
        transf = new Transform();
        hitbox = rect;

        r = new Random();
        aggro = r.nextBoolean();

        angle = (float) (r.nextFloat() * Math.PI * 2);
        geschw = r.nextFloat() * 2 + 1;

        this.points = points;

        spriteSheet = new SpriteSheet("res/Zombie/zombie_schlag_v2.png", 58, 54);
        schlag = new Animation(spriteSheet, new int[]{0, 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0}, times);
        deadZombie = new Image("res/deadZombie2.png");
    }

    /**
     * Zeichnet den Zombie
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public void draw(Graphics g) {
        if (alive) {
            if(pos == 0) {
                image.draw(x, y);
            }
            if(pos == 1) {
                schlag.draw(x - 16, y - 14.5f); //animation schlad grawing
            }
        }
        if(!alive){
            deadZombie.setRotation((float) (Math.toDegrees(angle)*Math.PI));
            deadZombie.draw(getX(),getY());
        }
        // image.setRotation((float) Math.toDegrees(angle));
    }

    /**
     * Wird in jedem update des Hauptkontainers ausgeführt
     *
     * @param delta int - Die Zeit die zwischen zwei frames vergangen ist
     * @param auto  Auto - Der Spieler
     */

    public void update(int delta, Auto auto) {
        if (alive) {
            if (y >= 1080) {
                y = 0;
            } else if (y < 0) {
                y = 1080;
            }
            if (x > 1920) {
                x = 0;
            } else if (x < 0) {
                x = 1920;
            }

            updatePos(auto);

            rect.setLocation(x, y);
            hitbox = rect.transform(transf.createRotateTransform((float) angle, getX() + 14.5f, getY() + 16));
            //aksbfiasuebfisuefb
        }
        if(!alive){
            //nichts
        }
    }

    /**
     * Updatelogik der Positionndes Zombies
     *
     * @param auto Auto - Der Spieler
     */
    public void updatePos(Auto auto) {
        if (aggro == true) { //Wenn der Zombie aggresiv auf den spieler ist
            autoPathFinder(auto);
            rotateToCar(auto);
        } else { //Sonst zufälliges Bewegungsmuster
            randomDirection(angle);
        }
    }

    /**
     * Lässt den Zombie in eine zufällige richtung gerade aus laufen.
     *
     * @param angle float - Winkel für die Berechnung der Richtung
     */
    private void randomDirection(float angle) {
        cos = Math.cos(angle) * geschw;
        sin = Math.sin(angle) * geschw;

        x = (int) (x - cos);
        y = (int) (y - sin);

        image.setRotation((float) Math.toDegrees(angle + Math.PI));
    }

    /**
     * Dreht das Bild des Zombies zum Auto
     *
     * @param auto Auto - Der Spieler
     */
    private void rotateToCar(Auto auto) {

        if ((auto.x > x) && (y > auto.y)) {
            a = x - auto.x;
            b = y - auto.y;
            angle = (float) Math.atan(b / a);
        }

        if ((auto.x > x) && (auto.y > y)) {
            a = auto.x - x;
            b = auto.y - y;
            angle += Math.PI;
            angle = (float) Math.atan(b / a);
        }

        if ((x > auto.x) && (auto.y > y)) {
            a = x - auto.x;
            b = auto.y - y;
            angle = (float) Math.atan(b / a);
            angle = (float) (Math.PI - angle);
        }

        if ((x > auto.x) && (y > auto.y)) {
            a = x - auto.x;
            b = y - auto.y;
            angle = (float) Math.atan(b / a);
            angle += Math.PI;
        }

        image.setRotation((float) Math.toDegrees(angle));
    }

    /**
     * Der Zombie verfolg hier den Spieler
     *
     * @param auto Auto - Der Spieler der verfolgt werden soll
     */
    private void autoPathFinder(Auto auto) {
        if (auto.x < x) {
            x--;
        } else if (auto.x > x) {
            x++;
        }
        if (auto.y < y) {
            y--;
        } else if (auto.y > y) {
            y++;
        }
    }

    /**
     * Sorgt dafür das der Zombie nach seinem Ableben auch verschwindet
     */
    public void die() {
        if (aliv == 0) {
            alive = false;
            points.addPoints();
            aliv = 1;
        }
    }

    public void setPos(int pos){
        this.pos = pos;
    }
    public boolean isAlive() {
        return alive;
    }
    public void getDamage(float dmg) {
        leben = leben - dmg;
    }
    public void setAggro(boolean aggro) {
        this.aggro = aggro;
    }
    public Shape getHitbox() {
        return hitbox;
    }
    public float getLeben() {
        return leben;
    }
    public void setDiedOnce(boolean diedOnce) {
        this.diedOnce = diedOnce;
    }
    public boolean getDiedOnce() {
        return diedOnce;
    }
}
