import org.newdawn.slick.Graphics;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by jusic on 31.03.2017.
 */
public class Punkte extends SpielObjekt {

    // VARIABLEN
    private int HIGHSCORE = 0;
    private int money;
    private ArrayList<Coin> coins;


    //OBJEKTE
    private FileWriter writer;
    private BufferedWriter bufferedWriter;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

    public Punkte() throws IOException {
    }

    public void draw(Graphics g) {
        g.drawString("Points: " + HIGHSCORE, 900, 10);
    }

    public void addToHighscoreList() {
        try {
            writer = new FileWriter("gamedata/highscore.txt", true);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(sdf.format(new Date()) + " " + HIGHSCORE);
            bufferedWriter.newLine();
            bufferedWriter.close();
            createSortedHighscoreList();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void createSortedHighscoreList() throws IOException {

        ArrayList<String[]> data = new ArrayList<String[]>();
        ArrayList<HighScore> hs = new ArrayList<>();
        writer = new FileWriter("gamedata/sortedhighscore.txt", true);
        bufferedWriter = new BufferedWriter(writer);

        data = readList("gamedata/highscore.txt");

        if(data.size() > 0 ){
            for(int i = 0; i < data.size(); i++){
                hs.add(new HighScore(data.get(i)[0] + data.get(i)[1], "", Integer.parseInt(data.get(i)[2])));
            }
        }

        hs.sort(HighScore.highScoreComperator);

        for(int i = 0; i < hs.size(); i++) {
            System.out.println(hs.get(i).getDate() + " " + hs.get(i).getHighscore());
        }
    }

    public ArrayList<String[]> readList(String filePath) {
        ArrayList<String[]> highscorelist = null;
        try {
            String read = "high score 123";
            highscorelist = new ArrayList<String[]>();
            FileReader fr = new FileReader(filePath);
            BufferedReader textReader = new BufferedReader(fr);

            do{
                read = textReader.readLine();
                if(read != null){
                    highscorelist.add(read.split(" "));
                }
            }while (read != null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return highscorelist;
    }


    public void addPoints() {
        HIGHSCORE += 10;
    }

    public void reset() {
        HIGHSCORE = 0;
    }

    public int getMoney() {
        return money;
    }

    public void addMoney(int amount) {
        money += amount;
    }
}

class HighScore implements  Comparable<HighScore>{

    private int highscore;
    private String date;
    private String dateDESC;

    public HighScore(String date, String dateDESC, int highscore){
        this.highscore = highscore;
        this.dateDESC = dateDESC;
        this.date = date;
    }


    public float getHighscore() {
        return highscore;
    }

    public void setHighscore(int highscore) {
        this.highscore = highscore;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateDESC() {
        return dateDESC;
    }

    public void setDateDESC(String dateDESC) {
        this.dateDESC = dateDESC;
    }

    public int compareTo(HighScore o) {
        float compareQuantity = ((HighScore) o).getHighscore();
        return (int) (compareQuantity - this.highscore);
    }

    public static Comparator<HighScore> highScoreComperator = new Comparator<HighScore>() {
        @Override
        public int compare(HighScore o1, HighScore o2) {
            String highsc1 = o1.getDate();
            String highsc2 = o2.getDate();

            return highsc1.compareTo(highsc2);
        }
    };

}