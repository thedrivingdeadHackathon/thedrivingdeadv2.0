import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

/**
 * Created by jusic on 31.03.2017.
 */
public class Herz extends SpielObjekt {

  /*  private TheDrivingDead theDrivingDead;
    private int checkstate = theDrivingDead.getGameStat(); */

    private Shape hitbox;
    private Rectangle rect;
    private boolean used = false;
    private float scale = 1;
    private boolean scaledir = true ;

    /**
     * Konstruktor des Herz Objektes
     *
     * @param x int - X-Koordinate des Objektes
     * @param y int - Y-Koordinate des Objektes
     * @param image Image - Bild dwelches das Objekt repräsentiert
     * @throws SlickException - Wenn ein fehler mit der Slick libary besteht wird dies ausgegeben
     */
    public Herz(int x, int y, Image image) throws SlickException {
        super(x, y, image);
        rect = new Rectangle(getX(), getY(), 28, 28);
        hitbox = rect;
    }

    /**
     * Hier wird bestimmt ob das herz gezeichnet soll oder nicht.
     * = Wenn das herz nicht benutzt ist => zeichne sonst nicht.
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public void draw(Graphics g) {

        if(!used){
            image.draw(x, y,scale);
        }
        //g.draw(rect);
       /* if (checkstate == 2) {
            image.draw(x, y);
        }c*/
    }

    /**
     * Wird in jedem loop ausgeführt. Sorgt dafür das das Herz ein wenig kleiner wird und anschließend wieder größer
     *
     * @param delta Zeitdifferenz zum letzten Aufruf
     */
    public void update(int delta) {
        if(scaledir){
            scale -= 0.01;
            if(scale <= 0.5){
                scaledir = false;
            }
        }
        if(!scaledir) {
            scale += 0.01;
            if(scale >= 1){
                scaledir = true;
            }
        }
    }

    public Shape getHitbox() {
        return hitbox;
    }
    public boolean isUsed() {
        return used;
    }
    public void setUsed(boolean used) {
        this.used = used;
    }
}
