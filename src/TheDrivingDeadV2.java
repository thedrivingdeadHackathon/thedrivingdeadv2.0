import org.newdawn.slick.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Main Class des Projekts.
 * mit VM options ausführen
 * -Djava.library.path=libs/libs-native
 * <p>
 * Created by Husein Jusic, Ermin Kameric, Sebastian Kainz on 31.03.2017.
 */
public class TheDrivingDeadV2 extends BasicGame{

    //menübuttons
    private Button start;
    private Button end;
    private Button shop;

    // SHOP BUTTON
    private Button leaveShop;
    private Button symbolVMAX;
    private Button symbolHandling;
    private Button symbolBeschl;

    private Button addVMAX;
    private Button addHANDL;
    private Button addBeschl;

    //GRUNDVARIABLEN
    private Image hintergrund;
    private Image coinDisplay;
    private Image logo;
    private Image GameOver;
    private Auto auto, freund;
    private ArrayList<Herz> herz = new ArrayList<>();
    private ArrayList<Coin> coin = new ArrayList<>();
    private ArrayList<Zombie> zombie = new ArrayList<>();
    private Punkte points;
    private ArrayList<Bullet> bullet = new ArrayList<>();
    private float timepast;

    //Sounds & Effekte
    private Sound[] sound = new Sound[6];
    //private Effekte effekte;

    //GAMELOGIC
    private int gameStat = 0;
    private boolean initialized = false;
    private int round = 0;
    private int anz = 0;
    private int coins = 0;

    //Lebensanzeige
    private final int lifeX = 1640;
    private final int lifeY = 20;
    private final int lifeBonusX = 1640;
    private final int lifeBonusY = 50;
    private Image[] leben = new Image[10];
    private Image[] lebenBonus = new Image[10];

    private Button startRound;

    public TheDrivingDeadV2() {
        super("The Driving Dead V2.0");
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer container = new AppGameContainer(new TheDrivingDeadV2());
        container.setDisplayMode(1920, 1080, true);
        container.setVSync(true);
        container.start();
    }


    public void init(GameContainer gameContainer) throws SlickException {
        auto = new Auto(100,100,new Image("res/dacia_Verde_klein.png"));
        freund = new Auto(500,500,new Image("res/dacia_Verde_klein.png"));
        hintergrund = new Image("res/background.jpg");
        coinDisplay = new Image("res/coin1.png");
        logo = new Image("res/menu/Logo.png");
        GameOver = new Image("res/menu/GameOver3.png");
        initLifeImages();
        initLifeBonusImages();
        initSounds();
        ServerHandler serverHandler = new ServerHandler(auto,freund);
        serverHandler.start();
        new Music("res/sounds/music.ogg").loop();
        try {
            points = new Punkte();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void update(GameContainer gameContainer, int delta) throws SlickException {
        //Auto updaten
        auto.update(delta);


        if (gameStat == 0) { // MENÜLOOP
            menuLoop();
            if (gameContainer.getInput().isKeyPressed(Input.KEY_ENTER)) {
                if (auto.getHitbox().intersects(start.getHitbox())) {
                    gameStat = 1;
                    round++;
                    initialized = false;
                }
                if (auto.getHitbox().intersects(end.getHitbox())) {
                    gameContainer.exit();
                }
            }
        }

        if (gameStat == 3) { // ROUNDENDLOOP
            if (gameContainer.getInput().isKeyPressed(Input.KEY_ENTER)) {
                if (auto.getHitbox().intersects(end.getHitbox())) {
                    gameContainer.exit();
                }
                if (auto.getHitbox().intersects(shop.getHitbox())) {
                    initialized = false;
                    gameStat = 4;
                    shopLoop();
                }
                if(auto.getHitbox().intersects(startRound.getHitbox())){
                    initialized = false;
                    gameStat = 1;
                }
            }
        }

        if(gameStat == 4){ // SHOPLOOP
            if (gameContainer.getInput().isKeyPressed(Input.KEY_ENTER)) {
                if(auto.getHitbox().intersects(leaveShop.getHitbox())){
                    gameStat = 3;
                }

                if(auto.getHitbox().intersects(addVMAX.getHitbox())){
                    if(coins >= 5 ){
                        auto.addVMAX();
                        coins -= 5;
                    }
                }
                if(auto.getHitbox().intersects(addBeschl.getHitbox())){
                    //AUTO.addbeschl
                }
                if(auto.getHitbox().intersects(addHANDL.getHitbox())){
                    if(coins >= 5 ){
                        auto.addHandl();
                        coins -= 5;
                    }
                }
            }
        }
        if (gameStat == 1) {
            gameLoop(delta);
        }
        if (gameStat == 2) {
            roundEndLoop(0); // round end loop
        }
        if (gameStat == 3) {
            menuLoop(delta);
        }


        if (gameContainer.getInput().isKeyPressed(Input.KEY_ESCAPE)) {
            gameContainer.exit();
        }

        if (gameContainer.getInput().isKeyDown(Input.KEY_W)) {
            auto.beschl();
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_S)) {
            auto.bremsen();
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_A)) {
            auto.rotate(1);
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_D)) {
            auto.rotate(-1);
        }
        if (!gameContainer.getInput().isKeyDown(Input.KEY_W)) {
            auto.bremsen();
        }
        if (gameContainer.getInput().isKeyPressed(Input.KEY_SPACE) && gameStat == 1) {
            sound[1].play();
            bullet.add(new Bullet(auto.getX() + 37.2f, auto.getY() + 16.5f, auto.getAngle(), zombie));  //getX()+37.2f,getY()+16.5f
        }
    }

    private void updateObjects(int delta) {
        auto.update(delta);
    }


    public void render(GameContainer gameContainer, Graphics g) throws SlickException {
        hintergrund.draw();
        auto.draw(g);
        //points.draw(g);

        if (gameStat == 1) {
            coinDisplay.draw(1860, 90);
            lebensanzeige();
            for (int i = 0; i < zombie.size(); i++) {
                zombie.get(i).draw(g);
            }
            // g.drawString(auto.getGeschw() + "", 1180, 50);
            // g.drawString(auto.getAngle() + "", 1180, 70);
            // g.drawString(timepast + "", 1180, 110);
            // g.drawString(anz + "", 1180, 130);
            // g.drawString(herz.size() + " Heart", 1180, 150);
            // g.drawString(coin.size() + " Coins-Possible", 1180, 170);
            //g.drawString("Auto Leben: " + auto.getLeben(), 50, 70);
            g.drawString(coins + "x", 1830, 100);
            g.drawString(round + " Round", 910, 25);
        }
        if (herz.size() > 0) {
            for (int i = 0; i < herz.size(); i++) {
                herz.get(i).draw(g);
            }
        }
        auto.draw(g);
        freund.draw(g);
        points.draw(g);

        if (coin.size() > 0) {
            for (int i = 0; i < coin.size(); i++) {
                coin.get(i).draw(g);
            }
        }

        if (bullet.size() > 0) {
            for (int i = 0; i < bullet.size(); i++) {
                bullet.get(i).draw(g);
            }
        }

        if (gameStat == 0 && start != null && end != null) {
            start.draw(g);
            end.draw(g);
            logo.draw(250, -300);

        }
        if (gameStat == 3 && shop != null && end != null) {
            shop.draw(g);
            startRound.draw(g);
            end.draw(g);
            GameOver.draw(550, 200);
        }
        if (gameStat == 4 && leaveShop != null) {
            coinDisplay.draw(1860, 90);
            g.drawString(coins + "x", 1830, 100);
            leaveShop.draw(g);
            symbolVMAX.draw(g);
            symbolHandling.draw(g);
            symbolBeschl.draw(g);

            addBeschl.draw(g);
            addVMAX.draw(g);
            addHANDL.draw(g);
        }
    }

    private void menuLoop() throws SlickException {
        if (!initialized) {
            start = new Button(820, 800, new Image("res/menu/Start.png"), 179, 150);
            end = new Button(1020, 800, new Image("res/menu/End.png"), 179, 150);
            initialized = true;
        }
    }

    public void gameLoop(int delta) throws SlickException {
        if (!initialized) {
            anz += round;
            for (int i = 0; i < anz; i++) {
                zombie.add(new Zombie((int) ((Math.random()) * 1920 + 1), (int) ((Math.random()) * 720 + 1), new Image("res/zombie.png"), points));
            }

            initialized = true;

        }
        if (initialized) {
            //Zombie updaten
            for (int i = 0; i < zombie.size(); i++) {
                zombie.get(i).update(delta, auto);
            }

            //BULLET UPDATE
            if (bullet.size() > 0) {
                for (int i = 0; i < bullet.size(); i++) {
                    bullet.get(i).update();
                }
            }
            if (timepast < 1000) {
                timepast = timepast + delta;
            }

            //Herz updaten
            if (herz.size() > 0) {
                for (int i = 0; i < herz.size(); i++) {
                    if (!herz.get(i).isUsed()) {
                        herz.get(i).update(delta);
                    }
                }
            }

            //AUTO AUF HERZ ?
            if (herz.size() > 0) {
                for (int i = 0; i < herz.size(); i++) {
                    if (!herz.get(i).isUsed()) {
                        if (auto.getHitbox().intersects(herz.get(i).getHitbox())) {
                            int amount = 40;

                            if (amount - round >= 10) {
                                auto.heal(amount - round);
                            } else {
                                auto.heal(10);
                            }
                            herz.get(i).setUsed(true);
                            sound[5].play();
                        }
                    }
                }
            }

            //Auto auf Münze?
            if (coin.size() > 0) {
                for (int i = 0; i < coin.size(); i++) {
                    if (!coin.get(i).isUsed()) {
                        if (auto.getHitbox().intersects(coin.get(i).getHitbox())) {
                            coins += 1;
                            coin.get(i).setUsed(true);
                            sound[3].play(); //COINS GET SOUND
                        }
                    }
                }
            }


            if (timepast > 500) {
                //Kollisionen abfragen
                for (int i = 0; i < zombie.size(); i++) {

                    if (auto.getHitbox().intersects(zombie.get(i).getHitbox()) && zombie.get(i).isAlive()) {
                        zombie.get(i).setPos(1);
                        //Zombie nimmt schaden + Auto nimmt schaden
                        if (auto.getGeschw() > 0) {
                            if (auto.getGeschw() > 3) {
                                zombie.get(i).getDamage(50);
                                auto.getDamage(10);
                            } else {
                                zombie.get(i).getDamage(25);
                            }
                            zombie.get(i).setAggro(true);
                        }
                        auto.getDamage(10);
                        timepast = 0;
                    } else {
                        zombie.get(i).setPos(0);
                    }
                }
            }

            // Schau ob irgendwas gestorben ist

            for (int i = 0; i < zombie.size(); i++) {

                if (zombie.get(i).getLeben() <= 0) {
                    zombie.get(i).die();


                    if (!zombie.get(i).getDiedOnce()) {
                        sound[4].play();
                        anz--;
                        zombie.get(i).setDiedOnce(true);

                        double rand = Math.random() * 5 + 1;
                        //System.out.println(rand+"");
                        if (rand > 5) { //HERZ SPAWN WAHRSCHEINLICHKEIT
                            herz.add(new Herz(zombie.get(i).getX(), zombie.get(i).getY(), new Image("res/heart.png")));
                        }

                        rand = Math.random() * 3 + 1;
                        if (rand > 2.5) {
                            coin.add(new Coin((int) ((Math.random()) * 1920 + 1), (int) ((Math.random()) * 1080 + 1), new Image("res/coin.png")));
                        }

                    }
                }

            }
            if (anz == 0) { // schauen ob zombies noch leben
                gameStat = 2;
                //  roundEndLoop(0); // round end loop
            }
            //Schau ob Auto noch am leben
            if (auto.getLeben() <= 0) {
                gameStat = 2; // game over loop
                roundEndLoop(1); // round end loop
            }


        }
    }

    /**
     * stat = 1 -> Player gestorben = Game over
     * stat = 0 -> Alle Zombies gestorben
     *
     * @param stat
     */
    public void roundEndLoop(int stat) throws SlickException {
        if (stat == 1) {
            //effekte.autoExplosion(auto.getX(), auto.getY());
            sound[0].play(); // AUTO EXPLOSION
            auto.reset();
            gameStat = 3;
            round = 0;
            anz = 0;
            //points.addToHighscoreList();
            points.reset();
            initialized = false;
            zombie.clear();
            bullet.clear();
            herz.clear();
            System.out.println("ROUND END LOOP 1 STARTED");
        }
        if (stat == 0) {
            round++;
            bullet.clear();
            gameStat = 1;
            initialized = false;
            sound[2].play();
            System.out.println("ROUND END LOOP 0 STARTED");

        }
    }
    private void menuLoop(int delta) throws SlickException {
        if (!initialized) {
            shop = new Button(950, 900, new Image("res/menu/shop.png"), 75, 90);
            startRound = new Button(200,900,new Image("res/menu/Start.png"),179,150);
            end = new Button(1750, 900, new Image("res/menu/End.png"), 179, 150);
            initialized = true;
        }
    }

    private void shopLoop() throws SlickException {
        if (!initialized) {
            leaveShop = new Button(100, 100, new Image("res/menu/shop/leave.png"), 50, 50);
            symbolVMAX = new Button(750,450,new Image("res/shop/vmax.png"), 179,150);
            symbolHandling = new Button(950,450, new Image("res/shop/hr.png"), 179,150);
            symbolBeschl = new Button(1150,450, new Image("res/shop/br.png"), 179,150);

            addVMAX = new Button(750,600,new Image("res/shop/add.png"),98,81);
            addHANDL = new Button(950,600,new Image("res/shop/add.png"),98,81);
            addBeschl = new Button(1150,600,new Image("res/shop/add.png"),98,81);
            initialized = true;
        }
    }


    private void initLifeImages() throws SlickException {
        leben[9] = new Image("res/lebensanzeige/leben10.png");
        leben[8] = new Image("res/lebensanzeige/leben9.png");
        leben[7] = new Image("res/lebensanzeige/leben8.png");
        leben[6]= new Image("res/lebensanzeige/leben7.png");
        leben[5] = new Image("res/lebensanzeige/leben6.png");
        leben[4] = new Image("res/lebensanzeige/leben5.png");
        leben[3]= new Image("res/lebensanzeige/leben4.png");
        leben[2]= new Image("res/lebensanzeige/leben3.png");
        leben[1]= new Image("res/lebensanzeige/leben2.png");
        leben[0] = new Image("res/lebensanzeige/leben1.png");
    }

    private void initLifeBonusImages() throws SlickException{
        lebenBonus[9] = new Image("res/lebensanzeige/leben_bonus_10.png");
        lebenBonus[8]= new Image("res/lebensanzeige/leben_bonus_9.png");
        lebenBonus[7] = new Image("res/lebensanzeige/leben_bonus_8.png");
        lebenBonus[6]= new Image("res/lebensanzeige/leben_bonus_7.png");
        lebenBonus[5] = new Image("res/lebensanzeige/leben_bonus_6.png");
        lebenBonus[4] = new Image("res/lebensanzeige/leben_bonus_5.png");
        lebenBonus[3]= new Image("res/lebensanzeige/leben_bonus_4.png");
        lebenBonus[2]= new Image("res/lebensanzeige/leben_bonus_3.png");
        lebenBonus[1]= new Image("res/lebensanzeige/leben_bonus_2.png");
        lebenBonus[0] = new Image("res/lebensanzeige/leben_bonus_1.png");
    }

    private void initSounds() throws SlickException {
        sound[0] = new Sound("res/sounds/explosion.wav");
        sound[1] = new Sound("res/sounds/schuss2.wav");
        sound[2] = new Sound("res/sounds/round_end.wav");
        sound[3] = new Sound("res/sounds/coin2.ogg");
        sound[4] = new Sound("res/sounds/death.wav");
        sound[5] = new Sound("res/sounds/heart.wav");
    }

    public void lebensanzeige() throws SlickException{
        //Lebenanzeige Herzen von 1-10 zeichnen bei bestimmten Lebenswert
        if (auto.getLeben() > 91){
            leben[9].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 81 && auto.getLeben() <= 91){
            leben[8].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 71 && auto.getLeben() <= 81){
            leben[7].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 61 && auto.getLeben() <= 71){
            leben[6].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 51 && auto.getLeben() <= 61){
            leben[5].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 41 && auto.getLeben() <= 51){
            leben[4].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 31 && auto.getLeben() <= 41){
            leben[3].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 21 && auto.getLeben() <= 31){
            leben[2].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 1 && auto.getLeben() <= 21){
            leben[1].draw(lifeX, lifeY);
        }
        else if (auto.getLeben() > 100 && auto.getLeben() <= 111){
            lebenBonus[0].draw(lifeBonusX, lifeBonusY);
        }
        if (auto.getLeben() > 111 && auto.getLeben() <= 121){
            lebenBonus[1].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 121 && auto.getLeben() <= 131){
            lebenBonus[2].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 131 && auto.getLeben() <= 141){
            lebenBonus[3].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 141 && auto.getLeben() <= 151){
            lebenBonus[4].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 151 && auto.getLeben() <= 161){
            lebenBonus[5].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 161 && auto.getLeben() <= 171){
            lebenBonus[6].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 171 && auto.getLeben() <= 181){
            lebenBonus[7].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 181 && auto.getLeben() <= 191){
            lebenBonus[8].draw(lifeBonusX, lifeBonusY);
        }
        else if (auto.getLeben() > 191 /*&& auto.getLeben() < 200 */){
            lebenBonus[9].draw(lifeBonusX, lifeBonusY);
        }
    }


}
