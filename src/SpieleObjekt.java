import org.newdawn.slick.*;

/**
 * Abstrakte Basisklasse für ein Spielobjekt (kann nicht instanziert werden).
 */
abstract class SpielObjekt {
    protected int x;
    protected int y;
    protected Image image;

    /**
     * Zeichnet das Spielobjekt - jede abgeleitete Klasse muss diese Methode implementieren (Abstrakte Definition -
     * --> Polymorphismus)
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public abstract void draw(Graphics g);

    /**
     * Wird bei jedem Spieldurchgang (Spiel-loop) aufgerufen.
     * Jede abgeleitete Klasse muss diese Methode implementieren (Abstrakte Definition -
     * --> Polymorphismus)
     *
     * @param delta Zeitdifferenz zum letzten Aufruf
     */
    public void update(int delta) {
    }

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @param image Graphik der Spielfigur
     */
    public SpielObjekt(int x, int y, Image image) {
        this(x, y);
        this.image = image;
    }

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param x x-Koordinate
     * @param y y-Koordinate
     */
    public SpielObjekt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Konstruktor mit Übergabewerten
     *
     * @param image Graphik der Spielfigur
     */
    public SpielObjekt(Image image) {
        this.image = image;
    }

    /**
     * Default-Konstruktor ohne Uebergabewerte
     */
    public SpielObjekt() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    };
}