import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

/**
 * Created by jusic on 31.03.2017.
 */
public class Auto extends SpielObjekt {

    private int VMAX = 5; //Maximalgeschw. des Autos
    private int HANDL = 20; //Kurvenradius
    private float leben = 100;

    private double angle = 0;
    private float rotateSpeed = 0.2f;
    private float beschleunigung = 1;
    private float geschw = 0;

    private double cos;
    private double sin;

    private Shape hitbox;
    private Rectangle rect;
    private Transform transf;

    /**
     * Konstruktor des Autos. Hier wird die position definiert und das Bild geladen
     *
     * @param x     int - x koordinate des Autos
     * @param y     int - y koorinate des Autos
     * @param image Image - Das Bild das das Auto darstellen soll
     */
    public Auto(int x, int y, Image image) {
        super(x, y, image);
        rect = new Rectangle(getX(), getY(), 75, 33);
        transf = new Transform();
        hitbox = rect;
    }

    /**
     * Diese funktion wird aus der Main gestartet. hier wird das Auto gezeichnet
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public void draw(Graphics g) {
        image.draw(x, y);
        image.setRotation((float) Math.toDegrees(angle));
        //g.draw(rect);
    }

    /**
     * Die Updatelogik des Autos
     *
     * @param delta Zeitdifferenz zum letzten Aufruf
     */
    public void update(int delta) {
        if (y >= 1080) {
            y = 0;
        } else if (y < 0) {
            y = 1080;
        }
        if (x > 1920) {
            x = 0;
        } else if (x < 0) {
            x = 1920;
        }

        cos = Math.cos(angle) * geschw;
        sin = Math.sin(angle) * geschw;

        x = (int) (x - cos);
        y = (int) (y - sin);

        rect.setLocation(x, y);
        hitbox = rect.transform(transf.createRotateTransform((float) angle, getX() + 37.2f, getY() + 16.5f));
    }

    /**
     * @return
     */
    public Shape getHitbox() {
        return hitbox;
    }

    /**
     * Die Klasse steuert und kontrolliert die Rotation des Autos.
     *
     * @param dir int - der Wert steht für die richtung ( 1 = Rechtskurve / -1 = Linkskurve )
     */
    public void rotate(int dir) {
        if (dir == -1) {
            rotateLeft();
        }
        if (dir == 1) {
            rotateRight();
        }
    }

    /**
     * Beschleunigungslogik des Autos
     */
    public void beschl() {
        if (geschw < VMAX) {
            geschw += beschleunigung;
        }
    }

    /**
     * Bremslogik des Autos
     */
    public void bremsen() {
        if (geschw > 0.1) {
            geschw -= beschleunigung / 2;
        }
    }

    /**
     * Wird aus der rotate funktion heraus geöffnet steuert das lanken nach links
     */
    private void rotateLeft() {
        if (angle > 2 * Math.PI) {
            angle = 0;
        }
        angle = angle + rotateSpeed * geschw / HANDL;
    }

    /**
     * Wird aus der rotate funktion heraus geöffnet steuert das lanken nach rechts
     */
    private void rotateRight() {
        if (angle < 0) {
            angle = 2 * Math.PI;
        }
        angle = angle - rotateSpeed * geschw / HANDL;
    }

    /**
     * Heilt das auto nachdem es ein Herz aufgesammelt hat um einen bestimten Wert
     *
     * @param amount int - Menge die geheilt werden soll
     */
    public void heal(int amount) {
        if (leben <= 200) {
            leben += amount;
            if(leben > 200){
                leben = 200;
            }
        }
    }

    /**
     * Setzt die Werte des Autos nach GAMEOVER wieder zurück
     */
    public void reset() {
        x = 100;
        y = 100;
        leben = 100;
    }

    /**
     * Lässt das Auto schaden nehmen
     *
     * @param dmg int - Wert des erlittenen schadens
     */
    public void getDamage(float dmg) {
        leben = leben - dmg;
    }

    public void addVMAX(){
        if(VMAX<= 10){
            VMAX += 1;
        }
    }

    public void addHandl(){
        if(HANDL >= 12){
            HANDL -= 1;
        }
    }
    public float getGeschw() {
        return geschw;
    }

    public double getAngle() {
        return angle;
    }

    public float getLeben() {
        return leben;
    }
}