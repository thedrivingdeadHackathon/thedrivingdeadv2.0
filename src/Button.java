import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

/**
 * Created by jusic on 31.03.2017.
 */
class Button extends SpielObjekt{

    private Shape shape;
    private Rectangle rect;

    private float a,b;

    /**
     * Konstruktor des Button Objektes
     *
     * @param x int - X-Koordinate des Objektes
     * @param y int - Y-Koordinate des Objektes
     * @param image Image - Bild dwelches das Objekt repräsentiert
     */
    public Button(int x, int y, Image image,float width, float height ){
        super(x,y,image);
        a = width;
        b = height;
        rect= new Rectangle(x-(a/2),y-(b/2),a,b);
        shape = rect;
    }

    /**
     * Zeichnet den Button
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public void draw(Graphics g) {
        image.drawCentered(x,y);
    }

    public Shape getHitbox(){
        return shape;
    }
}