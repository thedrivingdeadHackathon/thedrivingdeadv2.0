import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

import java.util.ArrayList;

/**
 * Created by jusic on 31.03.2017.
 */
public class Bullet extends SpielObjekt{

    public ArrayList<Zombie> zombie;

    public  final int RADIUS = 4;
    public  final int GESCHW = 10;
    public  final int DMG = 100;

    private double angle;

    private Shape hitbox;
    private Circle bullet;

    //HILFSVARS
    private double cos;
    private double sin;


    /**
     * Konstruktor des Button Objektes
     *
     * @param x int - X-Koordinate des Objektes
     * @param y int - Y-Koordinate des Objektes
     * @param zombie ArrayList - Liste der Zombies im Spiel
     */
    public Bullet(float x, float y, double angle, ArrayList zombie){
        super((int)x,(int)y);
        this.angle = angle;
        bullet = new Circle(getX(),getY(),RADIUS);
        hitbox = bullet;
        this.zombie = zombie;
    }

    /**
     * Zeichnet die Kugeln
     *
     * @param g Zugriff auf die Graphik des Spiels
     */
    public void draw(Graphics g) {
        g.draw(bullet);
    }

    /**
     * Steuert die richtung in der die Kugeln fliegen
     */
    public void update(){

        if(!(y >= 1080 || y < 0 || x > 1920 || x < 0)){
            cos = Math.cos(angle) * GESCHW;
            sin = Math.sin(angle) * GESCHW;

            x = (int) (x - cos);
            y = (int) (y - sin);

            bullet.setLocation(x, y);

            kollision();
        }

    }

    /**
     * Kollisionsabfrage zwischen Kugeln und Zombie
     */
    private void kollision() {
        for (int i = 0; i < zombie.size(); i++) {
            if(hitbox.intersects(zombie.get(i).getHitbox())){
                zombie.get(i).getDamage(100);
            }
        }
    }

    public Shape getHitbox(){
        return hitbox;
    }
}
